import React from 'react';
import {TableComposable, Thead, Tbody, Tr, Th, Td, ExpandableRowContent} from '@patternfly/react-table';
import { Card, CardBody, CardTitle, Checkbox } from '@patternfly/react-core';



export class ZuulTable extends React.Component{
    constructor(props){
        super(props)
    }
    
    render(){
        const ComposableTableExpandable = (rows) => {
            const columns = ['Job Name', 'Branch', 'Pipeline', 'Project', 'Result'];
            const rowPairs = [] 
            rows.rows.map(item => {rowPairs.push(
                {parent: [item.job_name, item.branch, item.pipeline, item.project, item.result]}
            )
            });
            //   { parent: ['one', 'two', 'a', 'four', 'five'], child: null },
            //   {
            //     parent: ['parent 1', 'two', 'k', 'four', 'five'],
            //     child: ['single cell']
            //   },
            const numColumns = columns.length;
            // Init all to true
            const [expanded, setExpanded] = React.useState(
              Object.fromEntries(Object.entries(rowPairs).map(([k, v]) => [k, Boolean(v.child)]))
            );
            const [compact, setCompact] = React.useState(true);
            const toggleCompact = checked => {
              setCompact(checked);
            };
            const handleExpansionToggle = (event, pairIndex) => {
              setExpanded({
                ...expanded,
                [pairIndex]: !expanded[pairIndex]
              });
            };
            let rowIndex = -1;
            return (
              <React.Fragment>
                <Checkbox
                  label="Compact"
                  isChecked={compact}
                  onChange={toggleCompact}
                  aria-label="toggle compact variation"
                  id="toggle-compact"
                  name="toggle-compact"
                />
                <TableComposable aria-label="Expandable Table" variant={compact ? 'compact' : null}>
                  <Thead>
                    <Tr>
                      <Th />
                      <Th>{columns[0]}</Th>
                      <Th>{columns[1]}</Th>
                      <Th>{columns[2]}</Th>
                      <Th>{columns[3]}</Th>
                      <Th>{columns[4]}</Th>
                    </Tr>
                  </Thead>
                  {rowPairs.map((pair, pairIndex) => {
                    rowIndex += 1;
                    const parentRow = (
                      <Tr key={rowIndex}>
                        <Td
                          key={`${rowIndex}_0`}
                          expand={
                            pair.child
                              ? {
                                  rowIndex: pairIndex,
                                  isExpanded: expanded[pairIndex],
                                  onToggle: handleExpansionToggle
                                }
                              : null
                          }
                        />
                        {pair.parent.map((cell, cellIndex) => (
                          <Td key={`${rowIndex}_${cellIndex}`} dataLabel={columns[cellIndex]}>
                            {cell}
                          </Td>
                        ))}
                      </Tr>
                    );
                    if (pair.child) {
                      rowIndex += 1;
                    }
                    const childRow = pair.child ? (
                      <Tr key={rowIndex} isExpanded={expanded[pairIndex] === true}>
                        {!rowPairs[pairIndex].fullWidth && <Td key={`${rowIndex}_0`} />}
                        {rowPairs[pairIndex].child.map((cell, cellIndex) => {
                          const numChildCells = rowPairs[pairIndex].child.length;
                          const shift = rowPairs[pairIndex].fullWidth ? 1 : 0;
                          const shiftedCellIndex = cellIndex + shift;
                          // some examples of how you could customize colSpan based on your needs
                          const getColSpan = () => {
                            // we have 6 columns (1 expandable column + 5 regular columns)
                            // for the rowPairs where we've specificed `fullWidth`, add +1 to account for the expandable column
                            let colSpan = 1;
                            if (numChildCells === 1) {
                              // single child cell: take up full width
                              colSpan = numColumns + shift;
                            } else if (numChildCells === 2) {
                              // 2 children
                              // child 1: 2 colspan
                              // child 2: 3 or 4 colspan depending on fullWidth
                              colSpan = cellIndex === 0 ? 2 : 3 + shift;
                            } else if (numChildCells === 3) {
                              // 3 children
                              // child 1: 2 colspam
                              // child 2: 2 colspan
                              // child 3: 1 or 2 colspan depending on fullWidth
                              colSpan = cellIndex === 2 ? 1 + shift : 2;
                            }
                            return colSpan;
                          };
                          return (
                            <Td
                              key={`${rowIndex}_${shiftedCellIndex}`}
                              dataLabel={columns[cellIndex]}
                              noPadding={rowPairs[pairIndex].noPadding}
                              colSpan={getColSpan()}
                            >
                              <ExpandableRowContent>{cell.title || cell}</ExpandableRowContent>
                            </Td>
                          );
                        })}
                      </Tr>
                    ) : null;
                    return (
                      <Tbody key={pairIndex} isExpanded={expanded[pairIndex] === true}>
                        {parentRow}
                        {childRow}
                      </Tbody>
                    );
                  })}
                </TableComposable>
              </React.Fragment>
            );
          };
        return(
            <Card>
                <CardTitle>
                    Zuul Jobs list
                </CardTitle>
                <CardBody>
                <ComposableTableExpandable rows={this.props.data}></ComposableTableExpandable>        
                </CardBody>
            </Card>
        )
    }
}