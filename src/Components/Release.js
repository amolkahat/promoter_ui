import React from 'react';
import { DescriptionList, DescriptionListDescription, DescriptionListGroup, DescriptionListTerm, Select, SelectOption, SelectVariant } from '@patternfly/react-core';
import { Card, CardBody, CardHeader, CardTitle } from '@patternfly/react-core';
import DescriptionGroup, {DescriptionCriteriaGroup} from './DescriptionList';
import SimpleDropdown from './Dropdown';
import releases from '../TestData/release'
import { DropdownItem } from '@patternfly/react-core';
import axios from 'axios';
import { TshirtIcon } from '@patternfly/react-icons';

class ReleaseClass extends React.Component {
    constructor(props) {
        super(props);
        this.setRelease.bind(this);
        this.updateData.bind(this);
        this.state = {
           release: [],
           selected: 'master',
           release_data : {},
        }
    }

    //https://www.geeksforgeeks.org/how-to-access-nested-object-in-reactjs/
    // https://medium.com/nerd-for-tech/making-a-nested-accordion-in-react-from-json-7d307b038f84
    // https://www.pluralsight.com/guides/represent-nested-objects-in-a-json-array-with-react.js-components

    componentDidMount(){
        this.getAllRelease()
        this.updateData()
    }

    getAllRelease(){
        axios.get('http://localhost:8000/api/release/')
        .then(response => {
            this.setState({release: response.data})
        })
    }
    getRelease(release_name){
        let data = this.state.release.map(r => {
            if (r.release === release_name){
                return r
            }
        })
        return data;
    }

    setRelease = (new_release) => {
        this.setState({selected: new_release})
        this.updateData()
    }

    updateData = () => {
        this.state.release.map(r => {
            console.log(this.state.selected)
            if (this.state.selected === r.release) {
                let data = this.getRelease(r.release);
                console.log("called UpdateData", data)
                this.setState({release_data: data})
            }
        });
    }

    // getRelease = (release) => {
    //     axios.get(`http://localhost:8000/api/release/${release}/`)
    //     .then(release=>{
    //         this.setState({release_data: release.data[0]})
    //         console.log("Get Release")
    //         console.log(release.data[0])
    //     })
    //     .catch(err=>console.log(err))
    // }

    getDropdownList = () => {
        const release_list = []
        this.state.release.map(item => {
            release_list.push(<DropdownItem key={item.release}>{item.release}</DropdownItem>)
        })
        return release_list
    }

    render() {
        const selected = this.state.selected;
        const r_data = this.state.release_data; //this.getRelease(selected);
        const PrintRelease = (props) => (<DescriptionList>
            <DescriptionCriteriaGroup term={"ID"} key={props.data.id} desc={props.data.id}/>
            <DescriptionGroup term={"Release"} key={props.data.release} desc={props.data.release}/>
            <DescriptionGroup term={"Source Namespace"} key={props.data.target_namespace + props.data.release}  desc={props.data.target_namespace}/>
            <DescriptionGroup term={"Target Namespace"} key={props.data.release + props.data.source_namespace} desc={props.data.source_namespace}/>
            <DescriptionGroup term={"Distro Name"} key={props.data.distro_name} desc={props.data.distro_name}/>
            <DescriptionGroup term={"Distro Version"} key={props.data.distro_version} desc={props.data.distro_version}/>
            <DescriptionGroup term={"DLRN HOST"} key={props.data.dlrn_api_host} desc={props.data.dlrn_api_host}/>
            <DescriptionGroup term={"DLRN API Slug"} key={props.data.dlrn_api_slug} desc={props.data.dlrn_api_slug}/>
            <DescriptionGroup term={"DLRN Base Slug"} key={props.data.dlrn_base_slug} desc={props.data.dlrn_base_slug}/>
            <DescriptionGroup term={"Hash Count"} key={props.data.latest_hashes_count} desc={props.data.latest_hashes_count}/>
        </DescriptionList>)
        return (
            <Card>
                <CardTitle>
                    Release <SimpleDropdown 
                                updateData = {this.updateData}
                                setRelease = {this.setRelease}
                                elements = {this.getDropdownList()}
                                name = {this.state.selected}   
                            />
                </CardTitle>
                <CardBody>
                    {/* {r_data.map((data, index) => {
                        if (typeof data !== 'undefined'){
                            return <PrintRelease data={data}/>}
                        }
                    )} */}
                </CardBody>
            </Card>
        )
    }
}

export default ReleaseClass;