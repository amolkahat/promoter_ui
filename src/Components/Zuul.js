import axios from 'axios';
import React from 'react';
import { Spinner } from '@patternfly/react-core';
import { ZuulTable } from '../screens/ZuulTable';
import IconAndTextTabs from '../Components/tabs';


class ZuulJobs extends React.Component{
    state = {
        zuul_jobs: [],
        isloading: false
    }
    
    // componentDidMount() {
    //     axios.get(`https://zuul.opendev.org/api/tenant/openstack/builds?limit=1000`)
    //     .then(res => {
    //         const zuul_jobs = res.data;
    //         this.setState({ 
    //             zuul_jobs: zuul_jobs,
    //             isloading: false 
    //         });
    //     })
    // }
    
    render() {
        if (this.state.isloading){
            return <Spinner isSVG></Spinner>
        }else {
            return (
                // <ZuulTable data={this.state.zuul_jobs}></ZuulTable>
                <IconAndTextTabs/>
            )
        }
        
    }
}

export default ZuulJobs;