import React from 'react';
import {Card, CardBody, CardFooter, CardTitle, ExpandableSection } from '@patternfly/react-core';
import SimpleDropdown from './Dropdown';
import CubesIcon from '@patternfly/react-icons/dist/esm/icons/cubes-icon';
import { Accordion, AccordionItem, AccordionContent, AccordionToggle } from '@patternfly/react-core';

  
class ComponentPromotions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false
    };
    this.onToggle = isExpanded => {
      this.setState({
        expanded: 'ex-toggle2'
      });
    };
  }

  render() {

    const onToggle = id => {
      if (id === this.state.expanded){
        this.setState({ expanded: ''})
      }else {
        this.setState({ expanded: id })
      }
    }


    const AccordionComp = props => (
      <Accordion asDefinitionList>
        <AccordionItem>
          <AccordionToggle
            onClick={() => {
              onToggle("ex-toggle1");
            }}
            isExpanded={this.state.expanded === "ex-toggle1"}
            id="ex-toggle1"
          >
          <CubesIcon></CubesIcon> a0b085d921d62d34dfe69dd5c9287f032f8d6a92
          </AccordionToggle>
          <AccordionContent id='ex-expand1' isHidden={this.state.expanded !== 'ex-toggle1'}>
            <p>
              This is the output
            </p>
          </AccordionContent>
        </AccordionItem>
      </Accordion>
    )

    return (
      <Card>
      <CardTitle>
        <SimpleDropdown></SimpleDropdown> 
      </CardTitle>
      <CardBody>
        <AccordionComp></AccordionComp>
      </CardBody>
      <CardFooter>
          
      </CardFooter>
    </Card>
    )
  }
}


export default ComponentPromotions;