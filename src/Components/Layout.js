import React from 'react';
import {
  Page,
  PageHeader,
  PageHeaderTools,
  PageSidebar,
  Nav,
  NavList,
  NavItem
} from '@patternfly/react-core';
import { BrowserRouter, Route, Routes, Link} from 'react-router-dom';
import ReleaseClass from './Release';

class VerticalPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isNavOpen: true,
      activeItem: 0,
    };
    this.onNavToggle = () => {
      this.setState({
        isNavOpen: !this.state.isNavOpen
      });
    };
    this.onSelect = result => {
        this.setState({
            activeItem: result.itemId
        });
    };
  }

  render() {
    const { isNavOpen, activeItem } = this.state;

    const logoProps = {
      href: 'https://patternfly.org',
      onClick: () => console.log('clicked logo'),
      target: '_blank'
    };
    const Header = (
      <PageHeader
        logo="Logo"
        logoProps={logoProps}
        headerTools={<PageHeaderTools>header-tools</PageHeaderTools>}
        showNavToggle
        isNavOpen={isNavOpen}
        onNavToggle={this.onNavToggle}
      />
    );
    
    const Navlist = (

        <Nav onSelect={this.onSelect}>
            <NavList>
                <NavItem iid="default-link1" to="#default-link1" itemId={0} isActive={activeItem === 0}>
                    <Link to="/">Home</Link>
                </NavItem>
                <NavItem iid="default-link2" to="#default-link2" itemId={1} isActive={activeItem === 1}>
                    <Link to='/release'>Release</Link>
                </NavItem>
                <NavItem iid="default-link3" to="#default-link3" itemId={2} isActive={activeItem === 2}>
                    <Link to='/promotion'>Promotions</Link>
                </NavItem>
                <NavItem iid='default-link4' to="#default-link4" itemId={3} isactive={activeItem === 3}>
                  <Link to="/zuul">Jobs</Link>
                </NavItem>
            </NavList>
        </Nav>
    );
    const Sidebar = <PageSidebar nav={Navlist} isNavOpen={isNavOpen}></PageSidebar>

    return (
    <BrowserRouter>
      <Page header={Header} sidebar={Sidebar}>
            <Routes>
                <Route exact path='/release' element={<ReleaseClass/>}>
                </Route>
            </Routes>
      </Page>
    </BrowserRouter>
    );
  }
}

export default VerticalPage;