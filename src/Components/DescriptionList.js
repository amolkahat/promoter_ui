
import React, { useState } from 'react';
import { DescriptionListDescription, DescriptionListTerm, DescriptionListGroup } from '@patternfly/react-core';

export default function DescriptionGroup(props){
    return(
         <DescriptionListGroup>
             <DescriptionListTerm>{props.term}</DescriptionListTerm>
             <DescriptionListDescription>{props.desc}</DescriptionListDescription>
         </DescriptionListGroup>
    );
 }

 export const DescriptionCriteriaGroup = (props) => {
    return(
        <DescriptionListGroup>
            <DescriptionListTerm>{props.term}</DescriptionListTerm>
            <DescriptionListDescription>{props.desc}</DescriptionListDescription>
            
        </DescriptionListGroup>
       
    )
 }
