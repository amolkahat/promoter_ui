import React from 'react';
import { Card, CardBody, Spinner } from '@patternfly/react-core';
import Basic from '../screens/EmptyState';
import axios from 'axios';
import ComposableTableExpandable from './ComposableExpandableTable';

class IconAndTextTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading: true, 
        isJenkinsLoading: true,
        activeTabKey: 0,
        jenkinsJobs: []
    };
    // Toggle currently active tab
    this.handleTabClick = (event, tabIndex) => {
      this.setState({
        activeTabKey: tabIndex
      });
    };
    this.getJenkinsElement = this.getJenkinsElement.bind(this);
  };

  componentDidMount(){
    console.log("prepare")
    
  }

  getZuulElement = () => {
      if (this.state.isLoading) {
          return <Spinner isSVG/>
      }else{
          return <h1>Zuul</h1>
      }
  }

  getJenkinsElement = () =>{
    if (this.state.isJenkinsLoading){
      return <Spinner isSVG/>
    }else {
      console.log("GetJenkinsElement", this.state.jenkinsJobs)
      return <h1>JenkinsJob</h1>
    }
  }

  render() {
    return (
      <Card>
        <CardBody>
          <ComposableTableExpandable jobsData={this.state.jenkinsJobs}/>
        </CardBody>
      </Card>
    );
  }
}

const ComposableTable  = () => {
  return <h1>Hello Table</h1>
}


export default IconAndTextTabs;