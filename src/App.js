import logo from './logo.svg';
import './App.css';
import VerticalPage from './Components/Layout';

function App() {
  return (
    <div className="App">
      <VerticalPage/>
    </div>
  );
}

export default App;
