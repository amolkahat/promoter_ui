import * as jenkinsActions from "../actions/jenkins";
import axios from "axios";
import { JENKINS_URL } from "../../config";

export const jenkinsReducers = {
    getData: (state = null, action) => {
        switch(action.type) {
            case jenkinsActions.FETCHING_DATA:
                axios.get(encodeURI(JENKINS_URL), {'Content-Type': 'application/json',}
                )
                .then((data) => {
                    this.setState({
                      isJenkinsLoading: false,
                      jenkinsJobs: data.data.jobs
                    })
                  })
                  .catch((err) =>{
                    this.setState({
                      isJenkinsLoading: false
                    })
                    console.log(err)
                  })
                return {
                    ...state,
                    loading: true,
                    error: null
                };
            case jenkinsActions.FETCHING_SUCCESS:
                return {
                    ...state,
                    loading: false,
                    items: action.payload.data
                };
            case jenkinsActions.FETCHING_FAILED:
                return {
                    ...state,
                    loading: false,
                    items: [],
                    error: action.payload.error
                };
        }
    }
}