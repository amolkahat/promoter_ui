import { createStore, combinReducers, applyMiddleware, compose, combineReducers } from "redux";

// TODO import reducers

import thunk from "redux-thunk"
import {routerMiddleware, routerReducer} from "react-router-redux";
import { jenkinsStore } from "./jenkinsStore";
import { appReducer } from "../reducers";


// TODO import other stores.

const initialState = {
    ...jenkinsStore
}

export const rootReducers = combineReducers({
    ...appReducer,
})

const composeEnhancers = 
    typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ 
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({

    })
    : compose;


const middleware = composeEnhancers(
    applyMiddleware(thunk)
);

export const store = createStore(rootReducers, initialState, middleware)