import {INCREMENT, DECREMENT} from './counter.types';

export const increseCounter = () => {
    return {
        type: INCREMENT,
    };
};

export const decreseCounter = () => {
    return {
        type: DECREMENT,
    };
};