import * as jenkins from "../actions/jenkins";

import {push} from "react-router-redux";

export const fetching_data = token => ({
    type: jenkins.FETCHING_DATA,
    payload: {token}
})

export const fetching_success = token => ({
    type: jenkins.FETCHING_SUCCESS,
    payload: {token}
})

export const fetching_failed = token => ({
    type: jenkins.FETCHING_FAILED,
    payload: {token}
})


