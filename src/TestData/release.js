const releases = [
    {
        "release": "master", 
        "api_url": "https://trunk.rdoproject.org/api-centos8-master-uc",
        "base_url": "https://trunk.rdoproject.org/centos8-master/",
        "distro_name": "centos", 
        "distro_version": 8, 
        "source_namespace": "tripleomaster", 
        "target_namespace": "tripleomaster", 
        "dlrn_api_host": "trunk.rdoproject.org", 
        "dlrn_api_scheme": "https", 
        "dlrn_api_port": "", 
        "latest_hashes_count": 100, 
        "manifest_push": false, 
        "ppc_manifests": false, 
        // "promotions": {
        //     "current-tripleo": {
        //         "candidate_label": "tripleo-ci-testing", 
        //         "criteria": [
        //             "periodic-tripleo-ci-build-containers-ubi-8-push", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-full-master", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-hardened-full-master", 
        //             "periodic-tripleo-centos-8-buildimage-ironic-python-agent-master", 
        //             "periodic-tripleo-ci-centos-8-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-scenario001-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-scenario002-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-scenario003-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-scenario004-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-scenario007-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-scenario010-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-scenario012-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-undercloud-containers-master", 
        //             "periodic-tripleo-ci-centos-8-containers-multinode-master",
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-api-master", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-scenario-master", 
        //             "periodic-tripleo-ci-centos-8-standalone-on-multinode-ipa-master", 
        //             "periodic-tripleo-ci-centos-8-undercloud-upgrade-master", 
        //             "periodic-tripleo-ci-centos-8-scenario000-multinode-oooq-container-updates-master", 
        //             "periodic-tripleo-ci-centos-8-scenario010-ovn-provider-standalone-master", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset001-master", 
        //             "periodic-tripleo-ci-centos-8-ovb-1ctlr_1comp-featureset002-master", 
        //             "periodic-tripleo-ci-centos-8-ovb-1ctlr_2comp-featureset020-master", 
        //             "periodic-tripleo-ci-centos-8-scenario007-multinode-oooq-container-master", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset035-master", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp_1supp-featureset039-master"
        //         ]
        //     }, 
        //     "current-tripleo-rdo": {
        //         "candidate_label": "current-tripleo", 
        //         "criteria": [
        //             "weirdo-master-centos8-promote-packstack-scenario001", 
        //             "weirdo-master-centos8-promote-packstack-scenario002", 
        //             "weirdo-master-centos8-promote-packstack-scenario003", 
        //             "weirdo-master-centos8-promote-puppet-openstack-scenario001", 
        //             "weirdo-master-centos8-promote-puppet-openstack-scenario002", 
        //             "weirdo-master-centos8-promote-puppet-openstack-scenario003", 
        //             "weirdo-master-centos8-promote-puppet-openstack-scenario004"
        //         ]
        //     }
        // }
    },
    {
        "release": "victoria", 
        "api_url": "https://trunk.rdoproject.org/api-centos8-victoria",
        "base_url": "https://trunk.rdoproject.org/centos8-victoria/", 
        "distro_name": "centos", 
        "distro_version": 8, 
        "source_namespace": "tripleovictoria", 
        "target_namespace": "tripleovictoria", 
        "dlrn_api_host": "trunk.rdoproject.org", 
        "dlrn_api_scheme": "https", 
        "dlrn_api_port": "", 
        "latest_hashes_count": 100, 
        // "promotions": {
        //     "current-tripleo": {
        //         "candidate_label": "tripleo-ci-testing",
        //         "criteria": [
        //             "periodic-tripleo-ci-build-containers-ubi-8-push-victoria", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-full-victoria", 
        //             "periodic-tripleo-centos-8-buildimage-ironic-python-agent-victoria", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-hardened-full-victoria", 
        //             "periodic-tripleo-ci-centos-8-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-standalone-upgrade-victoria", 
        //             "periodic-tripleo-ci-centos-8-undercloud-upgrade-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario001-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario002-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario003-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario004-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario007-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario010-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario012-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-api-victoria", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-scenario-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario010-ovn-provider-standalone-victoria", 
        //             "periodic-tripleo-ci-centos-8-standalone-on-multinode-ipa-victoria", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset001-victoria", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset035-victoria", 
        //             "periodic-tripleo-ci-centos-8-ovb-1ctlr_2comp-featureset020-victoria", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp_1supp-featureset039-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario007-multinode-oooq-container-victoria", 
        //             "periodic-tripleo-ci-centos-8-containers-multinode-victoria", 
        //             "periodic-tripleo-ci-centos-8-scenario000-multinode-oooq-container-updates-victoria", 
        //             "periodic-tripleo-ci-centos-8-undercloud-containers-victoria", 
        //             "periodic-tripleo-ci-centos-8-containers-undercloud-minion-victoria"
        //         ]
        //     }, 
        //     "current-tripleo-rdo": {
        //         "candidate_label": "current-tripleo", 
        //         "criteria": [
        //             "weirdo-victoria-centos8-promote-packstack-scenario001", 
        //             "weirdo-victoria-centos8-promote-packstack-scenario002", 
        //             "weirdo-victoria-centos8-promote-packstack-scenario003", 
        //             "weirdo-victoria-centos8-promote-puppet-openstack-scenario001", 
        //             "weirdo-victoria-centos8-promote-puppet-openstack-scenario002", 
        //             "weirdo-victoria-centos8-promote-puppet-openstack-scenario003", 
        //             "weirdo-victoria-centos8-promote-puppet-openstack-scenario004"
        //         ]
        //     }
        // }
    },
    {
        "release": "train", 
        "api_url": "https://trunk.rdoproject.org/api-centos8-train", 
        "base_url": "https://trunk.rdoproject.org/centos8-train/", 
        "distro_name": "centos", 
        "distro_version": 8, 
        "source_namespace": "tripleotraincentos8", 
        "target_namespace": "tripleotraincentos8", 
        "dlrn_api_host": "trunk.rdoproject.org", 
        "dlrn_api_scheme": "https", 
        "dlrn_api_port": "", 
        "latest_hashes_count": 100, 
        // "promotions": {
        //     "current-tripleo": {
        //         "candidate_label": "tripleo-ci-testing", 
        //         "criteria": [
        //             "periodic-tripleo-ci-build-containers-ubi-8-push-train", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-full-train", 
        //             "periodic-tripleo-centos-8-buildimage-ironic-python-agent-train", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-hardened-full-train", 
        //             "periodic-tripleo-ci-centos-8-undercloud-containers-train", 
        //             "periodic-tripleo-ci-centos-8-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-scenario001-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-scenario002-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-scenario003-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-scenario004-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-scenario007-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-scenario010-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-scenario012-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-api-train", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-scenario-train", 
        //             "periodic-tripleo-ci-centos-8-scenario007-multinode-oooq-container-train", 
        //             "periodic-tripleo-ci-centos-8-containers-multinode-train", 
        //             "periodic-tripleo-ci-centos-8-scenario000-multinode-oooq-container-updates-train", 
        //             "periodic-tripleo-ci-centos-8-scenario010-ovn-provider-standalone-train", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset001-train", 
        //             "periodic-tripleo-ci-centos-8-ovb-1ctlr_2comp-featureset020-train", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset035-train", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp_1supp-featureset039-train"
        //         ]
        //     }, 
        //     "current-tripleo-rdo": {
        //         "candidate_label": "current-tripleo", 
        //         "criteria": [
        //             "tripleo-quickstart-promote-train-current-tripleo-delorean-minimal", 
        //             "weirdo-train-centos8-promote-packstack-scenario001", 
        //             "weirdo-train-centos8-promote-packstack-scenario002", 
        //             "weirdo-train-centos8-promote-packstack-scenario003", 
        //             "weirdo-train-centos8-promote-puppet-openstack-scenario001", 
        //             "weirdo-train-centos8-promote-puppet-openstack-scenario002", 
        //             "weirdo-train-centos8-promote-puppet-openstack-scenario003"
        //         ]
        //     }
        // }
    },
    {
        "release": "ussuri", 
        "api_url": "https://trunk.rdoproject.org/api-centos8-ussuri", 
        "base_url": "https://trunk.rdoproject.org/centos8-ussuri/", 
        "distro_name": "centos", 
        "distro_version": 8, 
        "source_namespace": "tripleoussuri", 
        "target_namespace": "tripleou", 
        "dlrn_api_host": "trunk.rdoproject.org", 
        "dlrn_api_scheme": "https", 
        "dlrn_api_port": "", 
        "latest_hashes_count": 100, 
        // "promotions": {
        //     "current-tripleo": {
        //         "candidate_label": "tripleo-ci-testing", 
        //         "criteria": [
        //             "periodic-tripleo-ci-build-containers-ubi-8-push-ussuri", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-full-ussuri", 
        //             "periodic-tripleo-centos-8-buildimage-ironic-python-agent-ussuri", 
        //             "periodic-tripleo-ci-centos-8-standalone-ussuri", 
        //             "periodic-tripleo-ci-centos-8-scenario001-standalone-ussuri", 
        //             "periodic-tripleo-ci-centos-8-scenario002-standalone-ussuri", 
        //             "periodic-tripleo-ci-centos-8-scenario003-standalone-ussuri", 
        //             "periodic-tripleo-ci-centos-8-scenario004-standalone-ussuri", 
        //             "periodic-tripleo-ci-centos-8-scenario007-standalone-ussuri", 
        //             "periodic-tripleo-ci-centos-8-scenario012-standalone-ussuri", 
        //             "periodic-tripleo-ci-centos-8-standalone-on-multinode-ipa-ussuri", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-api-ussuri", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-scenario-ussuri",
        //             "periodic-tripleo-ci-centos-8-undercloud-containers-ussuri", 
        //             "periodic-tripleo-ci-centos-8-containers-multinode-ussuri", 
        //             "periodic-tripleo-ci-centos-8-containers-undercloud-minion-ussuri", 
        //             "periodic-tripleo-ci-centos-8-undercloud-upgrade-ussuri", 
        //             "periodic-tripleo-ci-centos-8-standalone-upgrade-ussuri", 
        //             "periodic-tripleo-ci-centos-8-scenario000-multinode-oooq-container-updates-ussuri", 
        //             "periodic-tripleo-ci-centos-8-scenario007-multinode-oooq-container-ussuri", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset001-ussuri", 
        //             "periodic-tripleo-ci-centos-8-ovb-1ctlr_2comp-featureset020-ussuri", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset035-ussuri", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp_1supp-featureset039-ussuri"
        //         ]
        //     }, 
        //     "current-tripleo-rdo": {
        //         "candidate_label": "current-tripleo", 
        //         "criteria": [
        //             "tripleo-quickstart-promote-ussuri-current-tripleo-delorean-minimal", 
        //             "weirdo-ussuri-centos8-promote-packstack-scenario001", 
        //             "weirdo-ussuri-centos8-promote-packstack-scenario002", 
        //             "weirdo-ussuri-centos8-promote-packstack-scenario003", 
        //             "weirdo-ussuri-centos8-promote-puppet-openstack-scenario001", 
        //             "weirdo-ussuri-centos8-promote-puppet-openstack-scenario002", 
        //             "weirdo-ussuri-centos8-promote-puppet-openstack-scenario003", 
        //             "weirdo-ussuri-centos8-promote-puppet-openstack-scenario004"
        //         ]
        //     }
        // }
    },
    {
        "release": "wallaby", 
        "api_url": "https://trunk.rdoproject.org/api-centos8-wallaby", 
        "base_url": "https://trunk.rdoproject.org/centos8-wallaby/", 
        "distro_name": "centos", 
        "distro_version": 8, 
        "source_namespace": "tripleowallaby", 
        "target_namespace": "tripleowallaby", 
        "dlrn_api_host": "trunk.rdoproject.org", 
        "dlrn_api_scheme": "https", 
        "dlrn_api_port": "", 
        "latest_hashes_count": 100, 
        "promotions": ''
        // "promotions": {
        //     "current-tripleo": {
        //         "candidate_label": "tripleo-ci-testing", 
        //         "criteria": [
        //             "periodic-tripleo-ci-build-containers-ubi-8-push-wallaby", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-full-wallaby", 
        //             "periodic-tripleo-centos-8-buildimage-ironic-python-agent-wallaby", 
        //             "periodic-tripleo-centos-8-buildimage-overcloud-hardened-full-wallaby", 
        //             "periodic-tripleo-ci-centos-8-standalone-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario001-standalone-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario002-standalone-wallaby",
        //             "periodic-tripleo-ci-centos-8-scenario003-standalone-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario004-standalone-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario007-standalone-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario010-standalone-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario012-standalone-wallaby", 
        //             "periodic-tripleo-ci-centos-8-undercloud-containers-wallaby", 
        //             "periodic-tripleo-ci-centos-8-containers-multinode-wallaby", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-api-wallaby", 
        //             "periodic-tripleo-ci-centos-8-standalone-full-tempest-scenario-wallaby", 
        //             "periodic-tripleo-ci-centos-8-standalone-on-multinode-ipa-wallaby", 
        //             "periodic-tripleo-ci-centos-8-undercloud-upgrade-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario000-multinode-oooq-container-updates-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario010-ovn-provider-standalone-wallaby", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp-featureset001-wallaby", 
        //             "periodic-tripleo-ci-centos-8-ovb-1ctlr_1comp-featureset002-wallaby", 
        //             "periodic-tripleo-ci-centos-8-ovb-1ctlr_2comp-featureset020-wallaby", 
        //             "periodic-tripleo-ci-centos-8-scenario007-multinode-oooq-container-wallaby", 
        //             "periodic-tripleo-ci-centos-8-ovb-3ctlr_1comp_1supp-featureset039-wallaby"
        //         ]
        //     }, 
        //     "current-tripleo-rdo": {
        //         "candidate_label": "current-tripleo", 
        //         "criteria": [
        //             "tripleo-quickstart-promote-wallaby-current-tripleo-delorean-minimal", 
        //             "weirdo-wallaby-centos8-promote-packstack-scenario001", 
        //             "weirdo-wallaby-centos8-promote-packstack-scenario002", 
        //             "weirdo-wallaby-centos8-promote-packstack-scenario003", 
        //             "weirdo-wallaby-centos8-promote-puppet-openstack-scenario001", 
        //             "weirdo-wallaby-centos8-promote-puppet-openstack-scenario002", 
        //             "weirdo-wallaby-centos8-promote-puppet-openstack-scenario003", 
        //             "weirdo-wallaby-centos8-promote-puppet-openstack-scenario004"
        //         ]
        //     }
        // }
    }
]


export default releases;