import React from 'react';
import {Button, Card, CardBody, CardTitle } from '@patternfly/react-core';
import {connect} from 'react-redux';
import {increseCounter, decreseCounter} from '../redux/Counter/counter.actions';

function HomeComponant(props) {
    return(
        <Card>
            <CardTitle></CardTitle>
            <CardBody>
                <h1> Card Body</h1>
                Count: {props.count} <br></br>
                <Button onClick={() => props.increseCounter()}> +1 </Button>
                <Button onClick={() => props.decreseCounter()}> -1 </Button>
            </CardBody>
        </Card>
    )
}

const mapStateToProps = (state) => {
    return {
        count: state.counter.count,
    };
};

const mapDispatchToProps = (dispath) => {
    return {
        increseCounter: () => dispath(increseCounter()),
        decreseCounter: () => dispath(decreseCounter()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponant);