from django.contrib import admin

# Register your models here.
from promoter.models import Release

admin.site.register(Release)