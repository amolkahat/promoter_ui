"""
"""
from django.db import models
from django.db.models import JSONField
# Create your models here.


class Release(models.Model):
    """
    """
    id = models.IntegerField(primary_key=True, unique=True)
    release = models.CharField(max_length=30, unique=True)
    distro_name = models.CharField(max_length=30,blank=False)
    distro_version = models.IntegerField(blank=False)
    dlrn_api_slug = models.CharField(max_length=200, blank=False)
    dlrn_base_slug = models.CharField(max_length=200, blank=False)
    dlrn_api_host = models.CharField(max_length=200, blank=False)
    dlrn_api_scheme = models.CharField(max_length=10, blank=False)
    dlrn_api_port = models.CharField(max_length=10, blank=False)
    source_namespace = models.CharField(max_length=50, blank=False)
    target_namespace = models.CharField(max_length=50, blank=False)
    latest_hashes_count = models.IntegerField(blank=False)
    # manifest_push = models.BooleanField()
    # ppc_manifest = models.BooleanField()
    promotions = JSONField()

    def __str__(self) -> str:
        return f"{self.release}".title()

#class Promotions(models.Model):
    
