from django.shortcuts import render
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from yaml import serialize

from promoter.models import Release
from promoter.serializers import ReleaseSerializer
from rest_framework.decorators import api_view
# Create your views here.


@api_view(['GET', 'POST','PUT', 'DELETE'])
def release_list(request):
    if request.method == 'GET':
        releases = Release.objects.all()
        serializer = ReleaseSerializer(releases, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'PUT':
        releases = JSONParser().parse(request)
        release_data = Release.objects.get(release=releases['release'])
        release_serializer = ReleaseSerializer(release_data, data=releases)
        if release_serializer.is_valid():
            release_serializer.save()
            return JsonResponse("Update Successfully", safe=False)

    elif request.method == 'POST':
        release_data = JSONParser().parse(request)
        serializer = ReleaseSerializer(data=release_data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def get_release(request, release):
    """
    """
    if request.method == "GET":
        try:
            release_data = Release.objects.filter(release=release)
            serializer = ReleaseSerializer(release_data, many=True)
            if serializer.data:
                return JsonResponse(serializer.data, safe=False)
            else:
                return JsonResponse({'message': f'Release: {release} does not exist'},
                                status=status.HTTP_404_NOT_FOUND)
        except Release.DoesNotExist:
            return JsonResponse({'message': 'Release does not exist'},
                                status=status.HTTP_404_NOT_FOUND)
