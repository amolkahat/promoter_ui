from django.urls import path
from promoter import views

urlpatterns = [
    path(r'release/', views.release_list),
    path(r'release/<str:release>/', views.get_release),
]
